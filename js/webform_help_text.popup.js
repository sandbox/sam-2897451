(function($) {
  /**
   * Toggle the help text popup.
   */
  Drupal.behaviors.webformHelpTextPopup = {
    attach: function(context, settings) {
      $('.webform-help-text-popup--clickable').once('webform-help-text-popup', function() {
        var $popup = $(this);
        $popup.find('.webform-help-text-popup__icon,.webform-help-text-popup__close').click(function() {
          $popup.toggleClass('webform-help-text-popup--clicked');
          return false;
        });
      });
    }
  };
})(jQuery);
