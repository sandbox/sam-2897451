<?php
/**
 * @file
 * Text popup template file.
 */
?>
<div class="webform-help-text-popup webform-help-text-popup--<?php print $activate_when; ?>">
  <div class="webform-help-text-popup__icon">
    ?
  </div>
  <div class="webform-help-text-popup__content">
    <a class="webform-help-text-popup__close" href="#">×</a>
    <div class="webform-help-text-popup__content-inner">
      <?php print $help_text; ?>
    </div>
  </div>
</div>
